# Nginx Purging Cache
## Nginx

## Run

- ```git clone https://gitlab.com/sytn1kk/nginx-purging-cache.git```
- ```cd nginx-purging-cache```
- ```docker-compose -f "docker-compose.yml" up -d --build ```

## Cache image on third request

```
curl -I http://localhost/200/300

HTTP/1.1 200 OK
Server: nginx/1.21.3
Date: Thu, 18 Nov 2021 17:48:27 GMT
Content-Length: 7832
Connection: keep-alive
cache-control: public, max-age=86400
expires: Thu, 31 Dec 2020 20:00:00 GMT
vary: User-Agent
access-control-allow-origin: *
Last-Modified: Thu, 18 Nov 2021 06:26:48 GMT
CF-Cache-Status: HIT
Age: 30726
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=w331PYwrlmoPHyVCqzdCMzbGGKxGUwP8i%2Fakcog6qhUrK%2BGFdEsJcGf5ylR0O0V1NI7wwSUHWh20S0Yguo2Ln3pGA6hKXQIBLISVooRIeoCSA4MiI8havF4lBPLYm1M6MvI%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
CF-RAY: 6b0307a0fc1377bf-KBP
alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400, h3-28=":443"; ma=86400, h3-27=":443"; ma=86400
Cache: MISS
Accept-Ranges: bytes
```

```
curl -I http://localhost/200/300
HTTP/1.1 200 OK
Server: nginx/1.21.3
Date: Thu, 18 Nov 2021 17:48:28 GMT
Content-Length: 7832
Connection: keep-alive
cache-control: public, max-age=86400
expires: Thu, 31 Dec 2020 20:00:00 GMT
vary: User-Agent
access-control-allow-origin: *
Last-Modified: Thu, 18 Nov 2021 06:26:48 GMT
CF-Cache-Status: HIT
Age: 30726
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=w331PYwrlmoPHyVCqzdCMzbGGKxGUwP8i%2Fakcog6qhUrK%2BGFdEsJcGf5ylR0O0V1NI7wwSUHWh20S0Yguo2Ln3pGA6hKXQIBLISVooRIeoCSA4MiI8havF4lBPLYm1M6MvI%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
CF-RAY: 6b0307a0fc1377bf-KBP
alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400, h3-28=":443"; ma=86400, h3-27=":443"; ma=86400
Cache: HIT
Accept-Ranges: bytes
```

## Purge cache

```
curl -I http://localhost/200/300 -H "cachepurge: true"

HTTP/1.1 200 OK
Server: nginx/1.21.3
Date: Thu, 18 Nov 2021 18:13:45 GMT
Content-Length: 7832
Connection: keep-alive
cache-control: public, max-age=86400
expires: Thu, 31 Dec 2020 20:00:00 GMT
vary: User-Agent
access-control-allow-origin: *
Last-Modified: Thu, 18 Nov 2021 06:26:48 GMT
CF-Cache-Status: HIT
Age: 32244
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Report-To: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=10aPKLJ4NlsxmwQyGUBDt7CUoK6VJSaMEL62qeFHNeRH6%2B0lrEeeoCQIjIKEzHmIIJXiM5QAS6ckhTsbMzC3yT%2BpThiulG7ZP%2FIFZYyO%2B42job6O4P2%2FwOEjdQOkE%2FsebZc%3D"}],"group":"cf-nel","max_age":604800}
NEL: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
CF-RAY: 6b032cb058c22d97-KBP
alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400, h3-28=":443"; ma=86400, h3-27=":443"; ma=86400
Cache: BYPASS
Accept-Ranges: bytes
```

## Screenshots
![Cache image on third request](https://i2.paste.pics/8b803ef4aa801c19205f8979307f0be3.png "Cache image on third request")
![Purge](https://i2.paste.pics/47560383c503bb4962d9215b16ef6863.png "Purge")

